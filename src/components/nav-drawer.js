/**
 * Created by hendy on 20/02/17.
 */
import { Component, PropTypes } from 'react'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import NavigationClose from 'material-ui/svg-icons/navigation/close'

export default class NavDrawer extends Component {
  handleSelect (view) {
    let { changeView, closeDrawer } = this.props
    changeView(view)
    closeDrawer()
  }

  render () {
    let { open, closeDrawer } = this.props
    return (
      <Drawer open={open}>
        <AppBar iconElementLeft={<IconButton onTouchTap={closeDrawer}><NavigationClose /></IconButton>} />
        <MenuItem onTouchTap={() => this.handleSelect('home')}>Home</MenuItem>
        <MenuItem onTouchTap={() => this.handleSelect('players')}>Players</MenuItem>
      </Drawer>
    )
  }
}