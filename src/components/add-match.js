/**
 * Created by hendy on 20/02/17.
 */
import { Component, PropTypes } from 'react'

import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import Toggle from 'material-ui/Toggle'

import _root from '../firebase'
import moment from 'moment'

export default class AddMatch extends Component {

  state = {
    err: null,
    teamAIsWinner: true,
    newMatch: {
      teamA: [ '', '' ],
      teamB: [ '', '' ]
    }
  };

  handleTextChange (team, pos, value) {
    let { newMatch } = this.state
    let copy = {}
    Object.assign(copy, newMatch)
    if (team === 'a') {
      copy.teamA[ pos ] = value
    }
    if (team === 'b') {
      copy.teamB[ pos ] = value
    }
    this.setState({
      newMatch: copy
    })
  }

  handleAdd () {
    let { newMatch, teamAIsWinner } = this.state
    let { teamA, teamB } = newMatch

    let aHasPlayer = !!teamA.join('')
    let bHasPlayer = !!teamB.join('')
    if (!aHasPlayer) {
      return this.setState({
        err: 'Team A needs at least one player!'
      })
    }

    if (!bHasPlayer) {
      return this.setState({
        err: 'Team B needs at least one player!'
      })
    }

    console.log('adding match')
    newMatch.date = moment().toISOString()
    newMatch.teamAIsWinner = teamAIsWinner
    _root.ref('matches').push(newMatch)
  }

  setWinner (teamAWins) {
    this.setState({
      teamAIsWinner: teamAWins
    })
  }

  render () {
    let { err, teamAIsWinner } = this.state
    return (
      <div style={{ marginBottom: 15 }}>
        <h2>Add a match result</h2>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', flexDirection: 'column', marginRight: 15 }}>
            <div>
              <h3>Team A</h3>
               <Toggle
                 label="Team A won"
                 labelPosition="right"
                 toggled={teamAIsWinner}
                 onToggle={() => this.setWinner(true)}
               />
            </div>
            <TextField id='teamA0' onChange={(e, val) => this.handleTextChange('a', 0, val)} />
            <TextField id='teamA1' onChange={(e, val) => this.handleTextChange('a', 1, val)} />
          </div>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div>
              <h3>Team B</h3>
              <Toggle
                label="Team B won"
                labelPosition="right"
                toggled={!teamAIsWinner}
                onToggle={() => this.setWinner(false)}
              />
            </div>
            <TextField id='teamB0' onChange={(e, val) => this.handleTextChange('b', 0, val)} />
            <TextField id='teamB0' onChange={(e, val) => this.handleTextChange('b', 1, val)} />
          </div>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <FlatButton primary onClick={::this.handleAdd}>Add</FlatButton>
          </div>
        </div>
        {err ? <h5 style={{ color: 'red' }}>{err}</h5> : null}
      </div>
    )
  }
}