/**
 * Created by hendy on 17/02/17.
 */

import { Component, PropTypes } from 'react'
import moment from 'moment'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'

const winningStyle = { backgroundColor: 'green' }

export default class LeagueTable extends Component {

  render () {
    let { matches, wlName } = this.props

    // build a list with the right win/loss ratios
    let list = []
    let wlNumerator = 0
    let wlDenominator = 0
    matches.sort((a, b) => moment(a.date).isAfter(moment(b.date))).forEach((match) => {
      if (match.teamA.includes(wlName)) {
        match.teamAIsWinner ? wlNumerator++ : wlDenominator++
      } else {
        match.teamAIsWinner ? wlDenominator++ : wlNumerator++
      }
      list.push(
        <TableRow key={match.id}>
          <TableRowColumn>{moment(match.date).fromNow()}</TableRowColumn>
          <TableRowColumn
            style={match.teamAIsWinner ? winningStyle : null}
          >
            {match.teamA.filter((name) => !!name).join(' & ')}
            </TableRowColumn>
          <TableRowColumn
            style={match.teamAIsWinner ? null : winningStyle}
          >
            {match.teamB.filter((name) => !!name).join(' & ')}
          </TableRowColumn>
          {wlName ? (
              <TableRowColumn>{wlNumerator}/{wlDenominator}</TableRowColumn>
            ) : null}
        </TableRow>
      )
    })

    return (
      <div style={{ marginBottom: 15 }}>
        <h3>League Table</h3>
        <Table selectable={false}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Time Played</TableHeaderColumn>
              <TableHeaderColumn>Team A</TableHeaderColumn>
              <TableHeaderColumn>Team B</TableHeaderColumn>
              {wlName ? <TableHeaderColumn>Win/Loss</TableHeaderColumn> : null}
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {list}
          </TableBody>
        </Table>
      </div>
    )
  }
}