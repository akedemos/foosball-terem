/**
 * Created by hendy on 17/02/17.
 */
import { Component, PropTypes } from 'react'
import Header from './header'
import Home from './home'
import NavDrawer from './nav-drawer'
import PlayerList from './player-list'
import _root from '../firebase'
import Paper from 'material-ui/Paper'

export default class App extends Component {
  state = {
    matches: [],
    view: 'home',
    drawerOpen: false
  };

  componentDidMount () {
    this.listenMatches()
  }

  addMatch (id, data) {
    let { matches } = this.state
    let existingMatch = matches.findIndex((m) => m.id === id)
    let match = { id, ...data }
    if (existingMatch > -1) {
      return this.setState({
        matches: [ ...matches.slice(0, existingMatch), match, ...matches.slice(existingMatch + 1) ]
      })
    }

    this.setState({
      matches: [ ...matches, { id, ...data } ]
    })
  }

  listenMatches () {
    _root.ref('matches').on('child_added', (snap) => {
      this.addMatch(snap.key, snap.val())
    })
  }

  openDrawer () {
    this.setState({
      drawerOpen: true
    })
  }

  closeDrawer () {
    this.setState({
      drawerOpen: false
    })
  }

  changeView (view) {
    this.setState({ view })
  }

  getViewComponent () {
    let { view, matches } = this.state
    let component

    let home = <Home matches={matches}/>
    let players = <PlayerList matches={matches}/>


    switch (view) {
      case 'home':
        component = home
        break
      case 'players':
        component = players
        break
      default:
        component = home
    }
    return component
  }

  render () {
    return (
      <Paper zDepth={0} style={{ width: '100%' }}>
        <Header openDrawer={::this.openDrawer} />
        <NavDrawer open={this.state.drawerOpen} closeDrawer={::this.closeDrawer} changeView={::this.changeView} />
        {this.getViewComponent()}
      </Paper>
    )
  }
}