/**
 * Created by hendy on 17/02/17.
 */
import { Component, PropTypes } from 'react'
import { List, ListItem } from 'material-ui/List'
import { uniq, flatten } from 'lodash/array'
import LeagueTable from './league-table'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'

export default class PlayerList extends Component {
  state = {
    player1: null,
    player2: null
  };

  constructor (...args) {
    super(...args)
    this.allPlayers = this.getPlayers()
  }

  getPlayers () {
    let { matches } = this.props
    return uniq(flatten(matches.map((match) => [ ...match.teamA, ...match.teamB ].filter((name) => !!name))))
  }

  setP1 (name) {
    this.setState({ player1: name })
  }

  setP2 (name) {
    this.setState({ player2: name })
  }

  componentWillUpdate () {
    this.allPlayers = this.getPlayers()
  }

  filterMatches (matches, name) {
    return matches.filter((match) => match.teamA.includes(name) || match.teamB.includes(name))
  }

  render () {
    let { matches } = this.props
    let { player1, player2 } = this.state

    if (player2) {
      //show p1 vs p2 stats
      let filtered = this.filterMatches(matches, player1)
      filtered = this.filterMatches(filtered, player2)
      return (
        <div>
          <h3>Showing stats for {player1} when playing against {player2}</h3>
          <SelectField
            floatingLabelText='Compare against another player'
            floatingLabelFixed
            onChange={(ev, key, value) => this.setP2(value)}
            value={player2}
          >
            {this.allPlayers.filter((n) => n !== player1).map((name) => (
              <MenuItem value={name} primaryText={name} key={name} />
            ))}
          </SelectField>
          <LeagueTable matches={filtered} wlName={player1} />
        </div>
      )
    }

    if (player1) {
      //show player one stats
      let filtered = this.filterMatches(matches, player1)
      return (
        <div>
          <h3>Showing Stats for {player1}</h3>
          <SelectField
            floatingLabelText='Compare against another player'
            floatingLabelFixed
            onChange={(ev, key, value) => this.setP2(value)}
          >
            {this.allPlayers.filter((n) => n !== player1).map((name) => (
              <MenuItem value={name} primaryText={name} key={name} />
            ))}
          </SelectField>
          <LeagueTable matches={filtered} wlName={player1} />
        </div>
      )
    }

    // display a list of all players
    return (
      <div>
        <h3>Click a player to see their stats</h3>
        <List>
          {this.allPlayers.map((name) => (
            <ListItem primaryText={name} key={name} onTouchTap={() => this.setP1(name)} />
          ))}
        </List>
      </div>
    )
  }
}