/**
 * Created by hendy on 20/02/17.
 */
import { Component, PropTypes } from 'react'
import LeagueTable from './league-table'
import AddMatch from './add-match'

export default class Home extends Component {
  render () {
    return (
      <div>
        <AddMatch />
        <LeagueTable matches={this.props.matches}/>
      </div>
    )
  }
}