/**
 * Created by hendy on 20/02/17.
 */
import { Component, PropTypes } from 'react'
import AppBar from 'material-ui/AppBar'

export default class Header extends Component{
  render () {
    return (
      <AppBar title="Foosball League" onLeftIconButtonTouchTap={() => this.props.openDrawer()} />
    )
  }
}