var webpack = require('webpack')
var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: [
    path.resolve(__dirname, 'src/index.js')
  ],
  resolve: {
    // When requiring, you don't need to add these extensions
    extensions: [ '.js', '.jsx' ]
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'foosball-teren.js'
  },
  target: 'web',
  module: {
    loaders: [
      // {
      //   include: path.resolve(__dirname, 'src/index.js'),
      //   loaders: [ 'expose?SplorBookingWidget', 'babel' ]
      // },
      {
        test: /\.json?$/,
        loader: 'json'
      },
      {
        test: /\.js?$/,
        include: path.resolve(__dirname, 'src'),
        exclude: [ /node_modules/ ],
        loader: 'babel-loader'
      }
    ]
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      // React available globally
      'React': require.resolve('react')
    }),
    new HtmlWebpackPlugin({
      title: 'foosball-teren',
      filename: 'index.html',
      template: './src/index.html',
      inject: 'body'
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    })
  ]
}