import myObject from '../src/index'
import {assert} from 'chai'

describe('module', () => {
  it('should have the correct properties', () => {
    assert.equal('ES6 Starter Kit', myObject.name)
  })
})
