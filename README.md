[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

# Foosball Terem Test

This repo is created to complete the foosball terem test

## Install

clone this repo and run
`
npm install
`

then
`
npm start
`

to launch a local copy of this site on port 8080

## License

All Rights Reserved
